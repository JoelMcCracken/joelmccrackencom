module Site.Glossary where

import qualified Data.Text       as T
import qualified Text.XML        as X
import qualified Text.XML.Cursor as C

findTermName :: C.Cursor -> Either T.Text T.Text
findTermName cursor =
  let
    nameElems =
      C.child cursor   >>=
      C.element "name" >>=
      C.child          >>=
      C.content
  in
    case nameElems of
      [] -> Left "glossary term specification error: No name found for term"
      [a] -> Right a
      _ -> Left "glossary term specification error: multiple names found for term"

findTermDefChildren :: X.Node -> [X.Node]
findTermDefChildren termNode =
  let
    cursor = C.fromNode termNode
    defNodeCs =
      C.child cursor  >>=
      C.element "def" >>=
      C.child
  in
    C.node <$> defNodeCs
