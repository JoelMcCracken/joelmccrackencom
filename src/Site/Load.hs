module Site.Load where

import           Data.Bifunctor
import qualified Data.Text.Lazy    as TL
import           Development.Shake
import           Prelude           hiding (readFile)
import qualified Relude            as R
import qualified Rib
import           Rib.Shake
import           Site.Parse        (parse)
import           Site.Types
import           System.FilePath   ((</>))
import qualified Text.XML          as X

loadSite :: Action Site
loadSite = do
  srcs <- Rib.forEvery ["*.xml"] readXmlFile
  case parse srcs of
    Left l  -> doWithErrors l
    Right r -> pure r

doWithErrors :: [(FilePath, ParseError)] -> Action Site
doWithErrors errors = error $ show errors

readXmlFile :: FilePath
            -> Action (FilePath, Either ParseError X.Document)
readXmlFile fp = do
  inputDir <- ribInputDir
  str <- R.toText <$> readFile' (inputDir </> fp)
  let edoc = first ParseErrorException (X.parseText X.def $ TL.fromStrict str)
  pure $ (fp, edoc)
