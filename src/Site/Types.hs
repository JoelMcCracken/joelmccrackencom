module Site.Types where

import           Control.Monad.Catch
import qualified Data.Map            as Map
import qualified Data.Text           as T
import           Data.Time.Calendar  (Day)
import           Prelude             hiding (readFile)
import qualified Text.XML            as X

data ParseError
  = ParseErrorException SomeException
  | ParseErrorText T.Text
  deriving Show

type Errors = [(FilePath, ParseError)]

data Site = Site
  { siteLinkTargets :: [LinkTarget]
  , siteSources     :: [Source]
  }
  deriving Show

data Source
  = Source
  { siteSourceFilePath   :: FilePath
  , siteSourceDocument   :: Document
  , siteSourceTOC        :: TOC
  , siteSourcePublishing :: Publishing
  , siteSourceLinkDest   :: LinkDest
  }
  deriving Show

data Publishing
  = Publishing
  { publishingState :: Maybe PublishingState
  , publishingDate  :: Maybe Day
  }
  deriving (Show, Eq)

data PublishingState
  = Draft
  | Published
  deriving (Show, Eq)

data TOC = TOC [TOCEntry]
  deriving Show

data TOCEntry = TOCEntry T.Text DocPartLinkTarget
  deriving Show

data Document
  = Document
    { documentTitle         :: Maybe T.Text
    , documentMetas         :: Metas
    , documentAliases       :: [Alias]
    , documentXmlDoc        :: X.Document
    , documentRoot          :: Node
    , documentInternalDests :: [T.Text]
    }
  deriving Show

data Node
  = ElementNode Element
  | ContentNode T.Text
  deriving (Show, Eq)

data Element
  = XmlElement XmlElementData
  | NoteElement NoteData
  | LinkElement LinkDest Node
  deriving (Show, Eq)

data NoteData =
  NoteData
  { noteDataNumber :: NoteNumber
  , noteDataName   :: NoteName
  , noteDataBody   :: [Node]
  }
  deriving (Eq, Show)

newtype NoteNumber
  = NoteNumber
  { unNoteNumber :: Int
  }
  deriving (Show, Eq)

newtype NoteName
  = NoteName
  { unNoteName :: T.Text
  }
  deriving (Eq, Show)

data XmlElementData
  = XmlElementData
  { elementName       :: T.Text
  , elementAttributes :: Map.Map T.Text T.Text
  , elementNodes      :: [Node]
  }
  deriving (Show, Eq)

-- TODO flatten the XMLM directly into "document", it doesnt really make sense to have separation
-- perhaps have a separate "source" type for it though because maybe someday will want to have
-- documents that are unattached from files? no, deal with that change when it comes

newtype DocName
  = DocName T.Text
  deriving (Eq, Show)

newtype DocAnchor
  = DocAnchor T.Text
  deriving (Eq, Show)

data LinkTarget
  = DocumentTop DocTopLinkTarget
  | DocumentPart DocPartLinkTarget
  deriving (Show)

data DocTopLinkTarget
  = DocTopLinkTarget LinkDest DocName FilePath
  deriving Show

data DocPartLinkTarget
  = DocPartLinkTarget LinkDest DocName DocAnchor FilePath
  deriving Show

newtype LinkDest
  = LinkDest T.Text
  deriving (Eq, Show)

type Alias = T.Text

type Metas = Map.Map X.Name T.Text
