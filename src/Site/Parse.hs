module Site.Parse (parse
                  , buildSiteFromSources
                  ) where

import           Control.Monad    (foldM)
import qualified Data.Map         as Map
import qualified Data.Text        as T
import           Data.Time.Format
import           Document
import qualified Document         as XM
import qualified LinkResolve      as LR
import qualified Note             as N
import           Prelude          hiding (readFile)
import qualified Relude           as R
import           RibUtil
import           Site.Types
import qualified Text.XML         as X

parse :: [(FilePath, Either ParseError X.Document)]
      -> Either [(FilePath, ParseError)] Site
parse srcs = do
  documents <- xmlDocumentToDocument srcs
  sources <- prepareSourcesFromDocs documents
  sources' <- N.resolveNotes sources
  site <- buildSiteFromSources sources'
  LR.resolveLinks site

xmlDocumentToDocument :: [(FilePath, Either ParseError X.Document)]
             -> (Either
                  [(FilePath, ParseError)]
                  [(FilePath, Document)])
xmlDocumentToDocument docs = do
  let
    partitionedDocs :: Either
                         [(FilePath, ParseError)]
                         [(FilePath, X.Document)]
    partitionedDocs = partitionEithers' $ distributeTuple <$> docs

    docToDocument :: (FilePath, X.Document)
              -> Either
                 (FilePath, ParseError)
                 (FilePath, Document)
    docToDocument (fp, doc) =
       distributeTuple $ (fp, XM.parseDocument doc)

    mapFPDocPairList :: [(FilePath, X.Document)]
                     -> Either [(FilePath, ParseError)]
                               [(FilePath, Document)]
    mapFPDocPairList fpds =
      partitionEithers' $ docToDocument <$> fpds

  partitionedDocs >>= mapFPDocPairList

buildSiteFromSources :: [Source] -> Either [(FilePath, ParseError)] Site
buildSiteFromSources sources = do
  targets <- prepareLinkTargetsFromSources sources
  pure $ Site targets sources

prepareTopLevelLinkTarget :: FilePath -> Either Errors LinkTarget
prepareTopLevelLinkTarget src = do
  linkDest <- R.first (:[]) $ articleLinkDest src
  pure $ DocumentTop (DocTopLinkTarget linkDest (DocName $ sourceBasename src) src)

prepareLinkTargetsFromSources :: [Source] -> Either Errors [LinkTarget]
prepareLinkTargetsFromSources sources =
  let
    foldSource :: [LinkTarget] -> Source -> Either Errors [LinkTarget]
    foldSource a (Source src doc _ _ _) = do
      tlTarget <- prepareTopLevelLinkTarget src
      parts <- getTargetsFromDocument src doc
      pure $ (tlTarget : parts) ++ a
  in
    foldM foldSource [] sources

prepareSourcesFromDocs :: [(FilePath, Document)] -> Either [(FilePath, ParseError)] [Source]
prepareSourcesFromDocs docs =
  let
    prepareSource' :: [Source]
                   -> (FilePath, Document)
                   -> Either [(FilePath, ParseError)] [Source]
    prepareSource' pss (src, document) = do
      case articleLinkDest src of
        Left l -> Left [l]
        Right linkDest -> do
          toc <- case getTocFromDocument src document of
                   Left l  -> Left l
                   Right r -> pure r
          publishing <- getPublishingInfoFromDocument src document
          let toc' = (TOC $ (uncurry TOCEntry) <$> toc)
          -- let publishing = undefined
          pure $ (Source src document toc' publishing linkDest) : pss
  in
    foldM prepareSource' [] docs

getPublishingInfoFromDocument :: FilePath -> Document -> Either [(FilePath, ParseError)] Publishing
getPublishingInfoFromDocument fp Document{..} = do
  let
    emState = case Map.lookup "pub-state" documentMetas of
      Nothing -> Right Nothing
      Just stateText ->
        case stateText of
          "draft" -> Right $ Just Draft
          "published" -> Right $ Just Published
          _ -> Left [(fp, ParseErrorText $ "Could not parse state '" <> stateText <> "', must be either 'draft' or 'published'")]
    mDateText = Map.lookup "pub-date" documentMetas
    emDay = case mDateText of
      Nothing       -> Right Nothing
      Just dateText -> do
        let
          parseResult = parseTimeM True defaultTimeLocale "%Y-%m-%d" (T.unpack dateText)
        case parseResult of
          Left l ->
            Left [(fp, ParseErrorText $ "Unable to parse date '" <> dateText <> "', should be in Y-m-d format, error from parser: " <> l)]
          Right day -> Right (Just day)
  mDay <- emDay
  mState <- emState
  pure $ Publishing mState mDay
