module RenderSite where

import           Control.Monad                   (join)
import qualified Data.Map                        as Map
import qualified Data.Maybe                      as M
import qualified Data.Text                       as T
import qualified Data.Traversable                as Trav
import           Prelude                         hiding (readFile)
import qualified Site.Glossary                   as G
import           Site.Types
import qualified Slug
import           Text.Blaze.Html                 (toHtml)
import           Text.Blaze.Html.Renderer.String (renderHtml)
import qualified Text.XML                        as X
import qualified Text.XML.Cursor                 as C

goElemElink :: Source -> Document -> (Map.Map X.Name T.Text) -> [X.Node] -> Either T.Text [X.Element]
goElemElink ps document attrs children =
  let
    toAttrName = "to"
    theError = error "elink tag is missing required 'to' attribute"
    mToAttr = Map.lookup toAttrName attrs
    target = M.fromMaybe theError mToAttr
    attrs' = (Map.fromList [("href", target)])
  in
    (pure . X.Element "a" attrs') <$> goElementChildren ps document children

goElemTerm :: Source -> Document -> X.Node -> Either T.Text [X.Element]
goElemTerm ps document node = do
  termTitle <- G.findTermName (C.fromNode node)
  termTitleSlug <- case Slug.sluggify termTitle of
    Nothing -> Left $ "Unable to sluggify " <> termTitle
    Just a  -> Right a
  let
    termAttrs = Map.fromList [ ("class", "glossary-term")
                             , ("id", termTitleSlug)
                             ]
    termDefChildren = G.findTermDefChildren node

    children :: Either T.Text [X.Node]
    children = goElementChildren ps document termDefChildren

    childrenDiv = divNode <$> children
  childrenDiv' <- childrenDiv
  let termTitleE :: X.Node
      termTitleE = emNode [X.NodeContent termTitle]

  pure $ [X.Element "li" termAttrs [termTitleE, childrenDiv']]

emNode :: [X.Node] -> X.Node
emNode children = X.NodeElement $ X.Element "em" mempty children

divNode :: [X.Node] -> X.Node
divNode children = X.NodeElement $ X.Element "div" mempty children

goElementChildren :: Source -> Document -> [X.Node] -> Either T.Text [X.Node]
goElementChildren ps document children =
  join <$> Trav.sequence (goNode ps document <$> children)

goNode :: Source -> Document -> X.Node -> Either T.Text [X.Node]
goNode ps document node@(X.NodeElement e) = (fmap . fmap) X.NodeElement (goElem ps document e node)
goNode _ _ (X.NodeContent t) = Right $ [X.NodeContent t]
goNode _ _ (X.NodeComment _) = Right $ [] -- hide comments
goNode _ _ (X.NodeInstruction _) = Right $ [] -- and hide processing instructions too

-- convert each source element to its XHTML equivalent
goElem :: Source -> Document -> X.Element -> X.Node -> Either T.Text [X.Element]
-- do not include any todos in output
goElem _ _ (X.Element "todo" _attrs _children) _ = Right $ []
-- do not include any aliases in output
goElem _ _ (X.Element "aliases" _attrs _children) _ = Right $ []
goElem ps document (X.Element "para" attrs children) _ =
  (pure . X.Element "p" attrs) <$> goElementChildren ps document children
goElem ps document (X.Element "em" attrs children) _ =
  (pure . X.Element "i" attrs) <$> goElementChildren ps document children
goElem ps document (X.Element "strong" attrs children) _ =
  (pure . X.Element "b" attrs) <$> goElementChildren ps document children
goElem ps document (X.Element "title" attrs children) _ =
  (pure . X.Element "h1" attrs) <$> goElementChildren ps document children
goElem ps document (X.Element "subtitle" attrs children) _ =
  (pure . X.Element "h2" attrs) <$> goElementChildren ps document children
goElem ps document (X.Element "introduction" attrs children) _ =
  (pure . X.Element "p" attrs) <$> goElementChildren ps document children
goElem ps document (X.Element "name" attrs children) _ =
  (pure . X.Element "h3" attrs) <$> goElementChildren ps document children
goElem ps document (X.Element "glossary" attrs children) _ =
  (pure . X.Element "ul" attrs) <$> goElementChildren ps document children
goElem ps document (X.Element "quote" attrs children) _ =
  (pure . X.Element "blockquote" attrs) <$> goElementChildren ps document children
goElem ps document (X.Element "js" attrs children) _ =
  (pure . X.Element "pre" attrs) <$> goElementChildren ps document children
goElem ps _document (X.Element "toc" _attrs _) _ =
  let
    Source _pth _src toc _publishing (LinkDest _dest) = ps
    TOC tocs = toc
    mkTocEntry :: TOCEntry -> X.Node
    mkTocEntry (TOCEntry txt (DocPartLinkTarget _ _ (DocAnchor anc) _)) =
      X.NodeElement $ X.Element "li" Map.empty  [X.NodeElement $ X.Element "a" (Map.fromList [("href", ( "#" <> anc ))]) [X.NodeContent txt]]
  in
    pure $ [X.Element "ul" (Map.fromList [("class", "toc")]) $ mkTocEntry <$> tocs]

goElem ps document (X.Element "term" _attrs _children) node =
  goElemTerm ps document node
goElem ps document (X.Element "elink" attrs children) _ =
  goElemElink ps document attrs children
goElem _ _ (X.Element "image" attrs _children) _ =
  Right $ pure $ X.Element "img" (fixAttr attrs) []
  where
    fixAttr mattrs
        | "href" `Map.member` mattrs  = Map.delete "href" $ Map.insert "src" (mattrs Map.! "href") mattrs
        | otherwise                 = mattrs
goElem ps document (X.Element name attrs children) _ =
  (pure . X.Element name attrs) <$> goElementChildren ps document children

renderRoot :: Source -> Document -> X.Node -> Either T.Text X.Element
renderRoot ps document rootNode =
  let
    children =
      case rootNode of
        X.NodeElement (X.Element _name _attrs children') -> children'
        _                                                -> []
  in
    (X.Element "div" Map.empty) <$> goElementChildren ps document children

renderSourceFile :: Source -> Document -> Either T.Text String
renderSourceFile ps document' =
  let
    doc = documentXmlDoc document'
    cursor = C.fromDocument doc
    node = C.node cursor
  in
    (renderHtml . toHtml . elementToDocument) <$> renderRoot ps document' node

elementToDocument :: X.Element -> X.Document
elementToDocument root' = X.Document (X.Prologue [] Nothing []) root' []
