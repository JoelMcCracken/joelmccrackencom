{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Document ( parseDocument
                , articleLinkDest
                , getTargetsFromDocument
                , getTocFromDocument) where

import           Control.Monad.Except
import           Control.Monad.State
import qualified Data.Map             as Map
import qualified Data.Text            as T
import qualified Data.Traversable     as Trav
import           Path
import           Prelude              hiding (readFile)
import qualified Relude               as R
import           RibUtil
import qualified Site.Glossary        as G
import           Site.Types
import qualified Slug
import qualified Text.XML             as X
import           Text.XML.Cursor      (($//))
import qualified Text.XML.Cursor      as C
-- KEEP ME:
-- import qualified Debug.Trace as DT

parseDocument :: X.Document -> Either ParseError Document
parseDocument doc =
  let
    aliases' = aliases doc
    metas' = metas doc
    title' = title doc
    linkDests = findInternalLinkDests doc
  in do
    root <- runExcept $ evalStateT (processDoc doc) 0
    R.bimap ParseErrorText (Document title' metas' aliases' doc root) linkDests

articleLinkDest :: FilePath -> Either (FilePath, ParseError) LinkDest
articleLinkDest srcPath = do
  let convertError x = (srcPath, ParseErrorException x)
  base' <- R.first convertError $ parseRelDir $ T.unpack $ sourceBasename srcPath
  pure $ LinkDest $ T.pack $ toFilePath $ [absdir|/entries|] </> base'

toDocPartLinkTarget' :: FilePath -> T.Text -> Either (FilePath, ParseError) DocPartLinkTarget
toDocPartLinkTarget' src txt = do
  anc <- R.first (\x-> (src, ParseErrorText x)) $ Slug.sluggifyE txt
  (LinkDest t) <- articleLinkDest src
  let dst = LinkDest $ t <> "#" <> anc
  pure $ DocPartLinkTarget dst (DocName $ sourceBasename src) (DocAnchor anc) src

toDocPartLinkTarget :: FilePath -> T.Text -> Either (FilePath, ParseError) LinkTarget
toDocPartLinkTarget src txt = DocumentPart <$> toDocPartLinkTarget' src txt

getTargetsFromDocument :: FilePath -> Document -> Either Errors [LinkTarget]
getTargetsFromDocument src doc =
  partitionEithers' $ toDocPartLinkTarget src <$> (documentInternalDests doc)

getTocFromDocument :: FilePath -> Document -> Either Errors [(T.Text, DocPartLinkTarget)]
getTocFromDocument src (Document _t _m _a doc _ _lds) = do
  tocEntries <- R.first (\x-> [(src, ParseErrorText x)]) $ findInternalLinkDests doc
  tocEntries' <- R.first (\x-> [(src, ParseErrorText x)]) $ Trav.sequence $ pairWithSlug <$> tocEntries
  let toDocPartTarget :: (T.Text, T.Text) -> Either (FilePath, ParseError) (T.Text, DocPartLinkTarget)
      toDocPartTarget (t, t') = do
        dplt <- toDocPartLinkTarget' src t'
        pure $ (t, dplt)
  partitionEithers' $ toDocPartTarget <$> tocEntries'

pairWithSlug :: T.Text -> Either T.Text (T.Text, T.Text)
pairWithSlug t = (t,) <$> Slug.sluggifyE t

processDoc :: forall m . MonadError ParseError m
           => MonadState Int m
           => X.Document
           -> m Node
processDoc doc = do
  let
    processNode :: X.Node -> m (Maybe Node)
    processNode node =
      case node of
        X.NodeElement elem' -> (Just . ElementNode) <$> processElement elem'
        X.NodeContent c     -> pure $ Just $ ContentNode c
        X.NodeInstruction _ -> pure $ Nothing  -- who cares
        X.NodeComment _     -> pure $ Nothing  -- who cares

    processNote attrs children = do
      children' <- R.catMaybes <$> processChildren children
      i <- get
      put (i + 1)
      name <-  liftEither $ maybeToEither (ParseErrorText "No name found on note") $
                 Map.lookup "name" attrs
      pure $ NoteData (NoteNumber i) (NoteName name) children'

    processElement :: X.Element -> m Element
    processElement (X.Element name attrs kidz) =
      case X.nameLocalName name of
        "note" -> NoteElement <$> processNote attrs kidz
        _ -> do
          let attrs' = Map.mapKeys X.nameLocalName attrs
              name' = X.nameLocalName name
          kidz' <- processChildren kidz
          pure $ XmlElement $ XmlElementData name' attrs' (R.catMaybes kidz')

    processChildren children = forM children processNode

  elem' <- processElement $ X.documentRoot doc
  pure $ ElementNode $ elem'

findGlossaryTermNodes :: X.Document -> [C.Cursor]
findGlossaryTermNodes doc =
  let
    cursor = C.fromDocument doc
  in
    cursor $//
      C.element "glossary" C.>=>
      C.child C.>=>
      C.element "term"

findInternalLinkDests :: X.Document -> Either T.Text [T.Text]
findInternalLinkDests doc = do
  Trav.sequence (G.findTermName <$> findGlossaryTermNodes doc)

aliases :: X.Document -> [Alias]
aliases doc = do
  let cursor = C.fromDocument doc
  let aliases' =
        C.child cursor      >>=
        C.element "aliases" >>=
        C.child             >>=
        C.element "alias"   >>=
        C.child             >>=
        C.content
  aliases'

title :: X.Document -> Maybe T.Text
title doc = do
  let cursor = C.fromDocument doc
  let title' =
        C.child cursor    >>=
        C.element "title" >>=
        C.child           >>=
        C.content
  R.listToMaybe title'

metas :: X.Document -> Map.Map X.Name T.Text
metas doc =
  let
    root' = X.documentRoot doc
  in
    X.elementAttributes root'
