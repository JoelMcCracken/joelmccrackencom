module RibUtil where

import           Data.Bifunctor
import qualified Data.Text       as T
import qualified Relude          as R
import           System.FilePath

sourceBasename :: FilePath -> T.Text
sourceBasename = T.pack . takeBaseName

maybeToEither :: b -> Maybe a -> Either b a
maybeToEither b Nothing  = Left b
maybeToEither _ (Just a) = Right a

distributeTuple :: Bifunctor f
                => (a, f b c)
                -> f (a, b) (a, c)
distributeTuple (a, e) = bimap (a,) (a,) e

partitionEithers' :: [Either a b] -> Either [a] [b]
partitionEithers' es =
  let (lefts, rights) = R.partitionEithers es
  in
    if length lefts > 0 then
      Left lefts
    else
      Right rights
