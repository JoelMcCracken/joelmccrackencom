module PreparedSite ( linkFromTarget,
                      lookupLink
                    ) where

import           Data.List  (find)
import qualified Data.Text  as T
import           RibUtil    (maybeToEither)
import           Site.Types hiding (TOC, TOCEntry)


linkFromTarget :: LinkTarget -> LinkDest
linkFromTarget (DocumentPart (DocPartLinkTarget dest _ _ _)) = dest
linkFromTarget (DocumentTop (DocTopLinkTarget dest _  _))    = dest

lookupLink :: Site -> T.Text -> Either T.Text LinkTarget
lookupLink (siteLinkTargets -> targets) txt =
  let
    x = T.splitOn " " txt
    errormsg = "No matching target found for: " <> txt
  in
    case x of
      [a]   -> maybeToEither errormsg $ findLinkTarget targets a Nothing
      [a,b] -> maybeToEither errormsg $ findLinkTarget targets a (Just b)
      _     -> Left "wrong number of parts"

findLinkTarget :: [LinkTarget] -> T.Text -> Maybe T.Text -> Maybe LinkTarget
findLinkTarget targets name anchor =
  find (doesTargetMatch (DocName name) (DocAnchor <$> anchor)) targets

doesTargetMatch :: DocName -> Maybe DocAnchor -> LinkTarget -> Bool
doesTargetMatch name' Nothing (DocumentTop (DocTopLinkTarget _ name _)) =
  name == name'
doesTargetMatch name' (Just anchor') (DocumentPart (DocPartLinkTarget _ name anchor  _)) =
  name == name' && anchor == anchor'
doesTargetMatch _ _ _ = False
