{-# LANGUAGE FlexibleContexts #-}

module Note (resolveNotes) where

import qualified Data.Text as T
import qualified Text.XML as X
import qualified Data.Map as Map
import Control.Monad.Except
import Control.Monad.State
import RibUtil (maybeToEither, partitionEithers')
import Site.Types
import qualified Relude as R
-- import qualified Debug.Trace         as DT

resolveNotes :: [Source] -> Either Errors [Source]
resolveNotes sources = do
  partitionEithers' (handleSource <$> sources)

handleSource :: Source
             -> Either (FilePath, ParseError) Source
handleSource (Source pth document toc publishing dst) = do
  -- TODO once again i think this needs to be fixed,
  --   siteSource should be correct by construction
  let documentToSource x = Source pth x toc publishing dst
  let includePath err = (pth, ParseErrorText err)
  let document' = runExcept $ evalStateT (prepareDoc document) 0
  R.bimap includePath documentToSource document'

prepareDoc :: MonadError T.Text m
           => MonadState Int m
           => Document
           -> m Document
prepareDoc document = do
  let
    doc = documentXmlDoc document
    node = X.documentRoot doc
    updateDocRoot rootElem =
      document { documentXmlDoc = doc { X.documentRoot = rootElem } }
  elem' <- prepareElement node
  pure $ updateDocRoot elem'

prepareElement :: MonadError T.Text m
               => MonadState Int m
               => X.Element
               -> m X.Element
prepareElement (X.Element "note" attrs children) = do
  children' <- prepareChildren children
  i <- get
  put (i + 1)
  let i' = T.pack $ show i
  name <-  liftEither $ maybeToEither "No name found on note" $
             Map.lookup "name" attrs

  let
    nodeBodyChildren = (X.NodeContent $ i' <> ". ") : children'

    noteBodyAttrs = Map.fromList [ ("class", "note-body")
                                 , ("id", name)
                                 ]
    supAttrs  = Map.fromList [("class", "note-num")]

    noteBodyElem = X.Element "span" noteBodyAttrs nodeBodyChildren

    noteSup = X.Element "sup" supAttrs [X.NodeContent i']

    noteElemWrap = X.Element "span" Map.empty [ X.NodeElement noteSup
                                              , X.NodeElement noteBodyElem
                                              ]
  pure noteElemWrap

prepareElement (X.Element tag attrs children) = do
  children' <- prepareChildren children
  pure $ (X.Element tag attrs children')

prepareChildren :: MonadError T.Text m
                  => MonadState Int m
                  => [X.Node]
                  -> m [X.Node]
prepareChildren children =
  forM children prepareNode

prepareNode :: MonadError T.Text m
            => MonadState Int m
            => X.Node
            -> m X.Node
prepareNode (X.NodeElement e) = X.NodeElement <$> prepareElement e
prepareNode x = pure $ x
