module Render.Styles where

import           Clay

pageStyle :: Css
pageStyle = "div#thesite" ? do
  margin (em 4) (pc 20) (em 1) (pc 20)
  "li.links" ? do
    listStyleType none
    marginTop $ em 1
    "b" ? fontSize (em 1.2)
    "p" ? sym margin (px 0)
