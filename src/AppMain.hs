module AppMain where

import qualified Clay
import           Control.Monad
import           Control.Monad.Catch
import qualified Data.Either         as E
import           Data.String         (fromString)
import           Data.Text           (Text)
import qualified Data.Text           as T
import qualified Data.Text.Lazy      as TL
import           Development.Shake
import           Lucid
import           Relude              (fromMaybe)
import qualified Render.Styles       as Styles
import qualified RenderSite          as RS
import qualified Rib
import           RibUtil             (sourceBasename)
import qualified Site.Load           as Load
import           Site.Types
import           System.FilePath
import           Text.RawString.QQ   (r)

-- KEEP ME:
-- import Debug.Trace (traceshow)

main :: IO ()
main = Rib.run "content" "dest" generateSite

generateSite :: Action ()
generateSite = do
  Rib.buildStaticFiles ["static/**"]
  site <- Load.loadSite
  writeHtmlFile "index.html" $ renderIndex site
  forM_ (siteSources site) writePage

writeHtmlFile :: FilePath -> Html () -> Action ()
writeHtmlFile path doc = do
  Rib.writeFileCached path $ TL.unpack $ Lucid.renderText  doc

articleCompiledDest :: MonadThrow m => FilePath -> m FilePath
articleCompiledDest srcPath = do
  let base' = T.unpack $ sourceBasename srcPath
  pure $ "entries" </> base' </> "index.html"

writePage :: Source -> Action ()
writePage ps@(Source pth _ _ _ _) = do
  dest <- liftIO $ articleCompiledDest pth
  writeHtmlFile dest $ renderPage ps

layout :: T.Text -> Html () -> Html ()
layout pageTitle pageBody =
  with html_ [lang_ "en"] $ do
  head_ $ do
    meta_ [httpEquiv_ "Content-Type", content_ "text/html; charset=utf-8"]
    title_ $ toHtml pageTitle
    link_ [href_ "/static/baskerville.css", rel_ "stylesheet"]
    link_ [href_ "/static/style.css", rel_ "stylesheet"]
    style_ [type_ "text/css"] $ Clay.render Styles.pageStyle
  body_ $ do
    with div_ [class_ "main"] $ do
       pageBody
       templateFooter
  termWith "script"
         [ async_ "true"
         , src_ "https://www.googletagmanager.com/gtag/js?id=UA-7759066-3"
         ]
         ""
  script_ [r|
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-7759066-3');
      |]

sourceIsPublished :: Source -> Bool
sourceIsPublished source =
  publishingState (siteSourcePublishing source) == Just Published

renderIndex :: Site -> Html ()
renderIndex site =
  let
    srcs = filter sourceIsPublished $ siteSources site

    srcTitle :: (FilePath, Document) -> String
    srcTitle (path, document) =
      T.unpack $ fromMaybe (T.pack path) $ documentTitle document

    renderLink :: Source -> Html ()
    renderLink (Source pth src _toc _ (LinkDest dest)) =
      with li_ [class_ "links"] $ do
        with a_ [href_ dest] $ fromString $ srcTitle (pth, src)

    renderIndexContent :: Html ()
    renderIndexContent = do
      let indexLink :: Text -> Html ()
          indexLink txt =
            with li_ [class_ "links"] $ with a_ [href_ $ "https://" <> txt] $ toHtml txt
      h1_ "Joel N. McCracken"
      h2_ "Entries"
      div_ $ ul_ $ forM_ srcs $ renderLink
      h2_ "Elsewhere"
      div_ $ do
        ul_ $ do
          indexLink "twitter.com/JoelMcCracken"
          indexLink "gitlab.com/JoelMcCracken"
          indexLink "github.com/JoelMcCracken"
          indexLink "joelmccracken.github.io"

    pageTitle :: T.Text
    pageTitle = "Joel N. McCracken"
  in
    layout pageTitle renderIndexContent

renderPage :: Source -> Html ()
renderPage ps@(Source path doc _toc _ _) =
  let
    pageTitle :: T.Text
    pageTitle =
      case documentTitle $ doc of
        Just title -> title
        Nothing    -> T.pack path
    rendered =
      -- TODO get rid of the `error` here; render all docs up front
      -- so can ensure have an Document
      toHtmlRaw $ (E.either (error . T.unpack) id) $ RS.renderSourceFile ps doc
  in
    layout pageTitle rendered

templateFooter :: Html ()
templateFooter = toHtmlRaw @Text [r|
      <div class="footer-wrap">
        <footer>
          <svg xmlns="http://www.w3.org/2000/svg"
               xmlns:xlink="http://www.w3.org/1999/xlink"
               version="1.0"
               viewBox="0 0 140 140"
               id="lambda">
            <g transform="translate(10,10)">
              <g id="scale" transform="scale(20,20)">
                <g id="grid" style="fill:none;stroke-linejoin:round;stroke-linecap:butt;stroke:#000000;stroke-width:.1;">
                  <!-- outside -->
                  <path d="m 0 0 L 6 0 L 6 6 L 0 6 Z" />

                  <!-- inside -->
                  <path d="M 0 2 L 6 2" />
                  <path d="M 0 4 L 6 4" />
                  <path d="M 2 0 L 2 6" />
                  <path d="M 4 0 L 4 6" />
                </g>
                <g id="dots" style="fill:#000000">
                  <ellipse cx="1" cy="1" rx=".7" ry=".7" id="C1" />
                  <ellipse cx="3" cy="3" rx=".7" ry=".7" id="C2" />
                  <ellipse cx="1" cy="5" rx=".7" ry=".7" id="C3" />
                  <ellipse cx="5" cy="5" rx=".7" ry=".7" id="C5" />
                </g>
              </g>
            </g>
          </svg>
          <div class="footer-info">
            <p>Joel N. McCracken</p>
            <p><a href="/">Home</a></p>
          </div>
        </footer>
      </div>
    |]
