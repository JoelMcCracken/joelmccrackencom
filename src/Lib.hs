module Lib where

import           Control.Monad.Catch
import           Data.Bifunctor
import qualified Data.Map            as Map
import qualified Data.Text           as T
import qualified Data.Text.Lazy      as TL
import           Development.Shake
import           Document
import qualified Path                as P
import           Prelude             hiding (readFile)
import           Relude              (toText)
import           Rib.Shake
import           Site.Types
import           System.FilePath     ((</>))

import qualified Text.XML            as X

-- KEEP ME:
-- import qualified Debug.Trace as DT

parseDocument :: FilePath -> Action (Either T.Text (FilePath, Document))
parseDocument f = do
  inputDir <- ribInputDir
  str <- toText <$> readFile' (inputDir </> f)
  let edoc = X.parseText X.def $ TL.fromStrict str
  let final = eitherErrorToText edoc
  let final' = Document.parseDocument =<< (first ParseErrorText final)
  pure $ first (T.pack . show) ((f,) <$> final')

eitherErrorToText :: Bifunctor b => b SomeException a -> b T.Text a
eitherErrorToText = first (T.pack . show)

getBaseFilename :: MonadThrow m0 => P.Path b P.File -> m0 (P.Path b P.File)
getBaseFilename pth =
  fst <$> P.splitExtension pth

nodeAttributes :: X.Node -> Maybe (Map.Map X.Name T.Text)
nodeAttributes =
  \case
    X.NodeElement e -> Just $ X.elementAttributes e
    _ -> Nothing

data GlossaryTerm
  = GlossaryTerm
      { glossaryTerm :: String
      , glossaryDef  :: X.Node
      }
