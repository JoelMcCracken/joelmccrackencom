module Slug where

import           Data.Char as C
import           Data.Text as T
import           RibUtil   (maybeToEither)

-- based off of https://hackage.haskell.org/package/slug-0.1.7/docs/src/Web-Slug.html#mkSlug

alphaOrSpace :: C.Char -> C.Char
alphaOrSpace x = if isAlphaNum x then x else ' '

sluggify :: T.Text -> Maybe T.Text
sluggify t =
  let
    ws = (T.words . T.toLower . T.map alphaOrSpace . T.replace "'" "") t
  in
    case ws of
      [] -> Nothing
      _  -> Just $ T.intercalate "-" ws

sluggifyE :: T.Text -> Either T.Text T.Text
sluggifyE t = maybeToEither ("Unable to sluffify " <> t) $ sluggify t
