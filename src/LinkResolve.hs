module LinkResolve where

import           Control.Monad (forM)
import qualified Data.Map      as Map
import qualified PreparedSite  as P
import qualified Relude        as R
import           RibUtil
import           Site.Types
import qualified Text.XML      as X
-- import Debug.Trace (traceShow)

resolveLinks :: Site -> Either Errors Site
resolveLinks site@(Site targets sources) = do
  srcs <- partitionEithers' $ handleSource' site <$> sources
  pure $ Site targets srcs

handleSource :: Site
             -> Source
             -> Either ParseError Source
handleSource site (Source srcPath document toc publishing dest) = do
  document' <- resolveLinksDocument site document
  -- TODO maybe not great that this transforms SIteSource to another Source
  -- we want it to be correct by construciton right?
  pure $ Source srcPath document' toc publishing dest

handleSource' :: Site
              -> Source
              -> Either (FilePath, ParseError) Source
handleSource' site src@(Source srcPath _ _ _ _) =
  R.first (srcPath,) $ handleSource site src

resolveLinksDocument :: Site -> Document -> Either ParseError Document
resolveLinksDocument site document = do
  let
    doc = documentXmlDoc document
    node = X.documentRoot doc
    updateDocRoot rootElem =
      document { documentXmlDoc = doc { X.documentRoot = rootElem } }
    he = handleElement site node
  updateDocRoot <$> he

handleElement :: Site -> X.Element -> Either ParseError X.Element
handleElement site (X.Element "link" attrs children) = do
  let
    children' = goElementChildren site children
    to = Map.lookup "to" attrs
    morphDest pLinkTarget cs =
      let
        (LinkDest ld) = P.linkFromTarget pLinkTarget
        scoot = X.Element "a" (Map.fromList [("href", ld)]) cs
      in
        pure scoot
    getDest dest cs =
      case P.lookupLink site dest of
        Left l  -> Left $ ParseErrorText ("Unable to find link target: " <> l)
        Right r -> morphDest r cs
  case to of
    Nothing -> Left $ ParseErrorText "link element missing 'to' attribute"
    Just a -> do
      cs <- children'
      getDest a cs

handleElement site (X.Element tag attrs children) = do
  children' <- goElementChildren site children
  pure $ (X.Element tag attrs children')

handleNode :: Site -> X.Node -> Either ParseError X.Node
handleNode site (X.NodeElement e) = X.NodeElement <$> handleElement site e
handleNode _ x                    = pure $ x

goElementChildren :: Site -> [X.Node] -> Either ParseError [X.Node]
goElementChildren site children =
  forM children (handleNode site)
