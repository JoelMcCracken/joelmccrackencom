import qualified AppMain
import qualified AppMain            as SM
import           Debug.Trace        (traceShow)
import           GHC.Exception.Type (SomeException)
import qualified Lib                as L
import           Path               (absfile)
import qualified PreparedSite       as P

import qualified Document           as XM
import           System.Random
import           Test.Hspec
import           Text.RawString.QQ  (r)
import qualified Text.XML           as X

import qualified Debug.Trace        as DT
import qualified Site.Parse         as L
import           Site.Types

sampleXML = [r|
  <post foo="bar"
        baz="123">
    <aliases>
      <alias>footy</alias>
    </aliases>
    <glossary>
      <term>
        <name>monad</name>
        <def>scoot</def>
      </term>
    </glossary>
  </post>
|]

-- TODO move all this sample data to test subdirs and bring the code
--      that handles file reading/loading under test
eDoc :: Either GHC.Exception.Type.SomeException X.Document
eDoc = X.parseText X.def sampleXML

mkFixture :: Either [(FilePath, ParseError)] P.PreparedSite
mkFixture = do
  let
    (Right doc1) = eDoc
    (Right doc1') = XM.parseDocument doc1
    p1 = ("/tmp/blahpost.xml", doc1')
  site <- L.buildSiteFromSources [p1]
  P.prepareSite site

main :: IO ()
main = do
  let (Right doc) = eDoc
  let (Right fixture) = mkFixture
  hspec $ do
    it "find aliases" $ do
      XM.aliases doc `shouldBe` ["footy"]
    describe "finding link dests" $ do
      it "finds glossary terms" $ do
        let (Right fild) = XM.findInternalLinkDests doc
        head fild `shouldBe` "monad"

    describe "do link lookup" $ do
      it "creates links list in expected format" $ do
        (length $ P.pLinkTargets $ fixture) `shouldBe` 2

        let (DocumentTop
              (DocTopLinkTarget
                (LinkDest dest)
                (DocName txt)
                src))
              = (P.pLinkTargets fixture) !! 0
        txt `shouldBe` "blahpost"
        dest `shouldBe` "/entries/blahpost/"

        let (DocumentPart
              (DocPartLinkTarget
                (LinkDest dest)
                (DocName txt)
                (DocAnchor anc)
                src))
              = (P.pLinkTargets fixture) !! 1

        txt `shouldBe` "blahpost"
        anc `shouldBe` "monad"
        dest `shouldBe` "/entries/blahpost/#monad"

      it "is able to look them up" $ do
        (length $ P.pLinkTargets fixture) `shouldBe` 2

        let Right (DocumentPart
                    (DocPartLinkTarget
                      (LinkDest "/entries/blahpost/#monad")
                      (DocName txt)
                      (DocAnchor anc)
                      src))
              = P.lookupLink fixture "blahpost monad"

        txt `shouldBe` "blahpost"
        anc `shouldBe` "monad"

        let Right (DocumentTop
                    (DocTopLinkTarget
                      (LinkDest "/entries/blahpost/")
                      (DocName txt)
                      src))
              = P.lookupLink fixture "blahpost"

        txt `shouldBe` "blahpost"

        let Left err = P.lookupLink fixture "blahpost monadd"
        err `shouldBe` "No matching target found for: blahpost monadd"

        let Left err = P.lookupLink fixture "blahpost monadd foo"
        err `shouldBe` "wrong number of parts"
