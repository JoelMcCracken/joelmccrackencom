serve:
	nix-shell --run 'ghcid -T ":main -ws :8080"'

repl:
	nix-shell --run "cabal repl"

test:
	nix-shell --run "ghcid -c 'cabal repl joelmccrackencom-test' --test 'main'"

.PHONY: test
